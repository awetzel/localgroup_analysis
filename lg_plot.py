'''
Analysis of galaxies in the Local Group.

@author: Andrew Wetzel <arwetzel@gmail.com>

----------
Units

Unless otherwise noted, this package stores all quantities in (combinations of) these base units
    mass [M_sun]
    position [kpc comoving]
    distance, radius [kpc physical]
    time [Gyr]
    temperature [K]
    magnetic field [Gauss]
    elemental abundance [linear mass fraction]

These are the common exceptions to those standards
    velocity [km/s]
    acceleration [km/s / Gyr]
    gravitational potential [km^2 / s^2]
    rates (star formation, cooling, accretion) [M_sun / yr]
    metallicity (if converted from stored massfraction)
        [log10(mass_fraction / mass_fraction_solar)], using Asplund et al 2009 for Solar
'''

import numpy as np

# from matplotlib import pyplot as plt
from matplotlib import ticker

import utilities as ut

# --------------------------------------------------------------------------------------------------
# utility
# --------------------------------------------------------------------------------------------------
host_label = {
    'both': {
        'point': 'pp',
        'color': 'blue',
        'expand': 3.0,
        'weight': 6.0,
        'offset': 0,
        'caption': 'MW+M31',
        'ptype': None,
    },
    'MW': {
        'point': 'pp',
        'color': 'violet',
        'expand': 2.5,
        'weight': 5.0,
        'offset': 0.2 * -1,
        'caption': 'MW',
        'ptype': 43,
    },
    'M31': {
        'point': 'pp',
        'color': 'green',
        'expand': 2.5,
        'weight': 5.0,
        'offset': 0.2,
        'caption': 'M31',
        'ptype': 33,
    },
}


# --------------------------------------------------------------------------------------------------
# analysis
# --------------------------------------------------------------------------------------------------
def plot_quiescent_fraction_v_mass(
    gal,
    mass_name='star.mass',
    mass_limits=[1e3, 1e10],
    mass_width=1,
    distance_max=300,
    sub_host_names=['both', 'MW', 'M31'],
    gas_mass_ratio_qu=0.1,
):
    '''
    Plot quiescent fraction v mass_name.

    Parameters
    ----------
    gal : dict
        catalog of galaxies in local group
    mass_name : str
        galaxy mass kind
    mass_limits : list
        min and max limits for mass_name
    mass_width : float
        width of mass bin
    distance_max : float
        maximum distance from host [kpc physical]
    sub_host_names : str or list
        name[s] of hosts: None, MW, M31
    gas_mass_ratio_qu : float
        maximum ratio of gas.mass / star.mass to be quiescent
    '''
    if np.isscalar(sub_host_names):
        sub_host_names = [sub_host_names]

    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    QuFrac = ut.math.FractionClass([len(sub_host_names), MassBin.number], uncertainty_kind='beta')

    gis = ut.array.get_indices(gal[mass_name], mass_limits)
    gis = ut.array.get_indices(gal['host.distance.total'], [0, distance_max], gis)

    for host_i, host_name in enumerate(sub_host_names):
        if host_name == 'both':
            gis_h = gis
        else:
            gis_h = ut.array.get_indices(gal['host.name'], host_name, gis)

        for mi in range(MassBin.number):
            mass_bin_limits = MassBin.get_bin_limits(mi)
            gis_m = ut.array.get_indices(gal[mass_name], mass_bin_limits, gis_h)
            gis_m_qu = ut.array.get_indices(gal['gas.mass.ratio'], [0, gas_mass_ratio_qu], gis_m)
            QuFrac.assign_to_dict([host_i, mi], gis_m_qu.size, gis_m.size)

        print(QuFrac['denom'][host_i])
        print(QuFrac['value'][host_i])

    if np.max(mass_limits) == 10:
        MassBin.mids[-1] = 9.18  # LMC is the only galaxy at this mass

    # plot ----------


def plot_quench_time_v_mass(
    sub,
    gal,
    mass_name='star.mass',
    mass_limits=[1e4, 1e9],
    mass_width=1,
    use_distance_cut=True,
    infall_property_name='sat.host.t',
    gal_host_names=['both'],
    host_paired=None,
    simulation_names_exclude=None,  # ['Siegfried&Roy', 'Serena&Venus'],
    gas_mass_ratio_qu=0.1,
):
    '''
    Plot quenching time of satellites versus mass_name.

    Parameters
    ----------
    sub : dict : catalog of subhalos at z = 0
    gal : dict : catalog of galaxies in local group
    mass_name : str : galaxy mass kind
    mass_limits : list : min and max limits for mass_name
    mass_width : float : width of mass bin
    use_distance_cut : float : whether to use only satellites within 300 kpc physical
    infall_property_name : str : property to determine time since infall
    gal_host_names : str or list : name[s] of observed hosts: both, MW, M31
    host_paired : bool : whether to use paired or isolated hosts in ELVIS: None, True, False
    simulation_names_exclude : str or list : names of simulations to exclude in ELVIS
    gas_mass_ratio_qu : float : maximum ratio of gas.mass / star.mass to be quiescent
    file_name : str
        name of plot file to write
    '''
    from elvis_analysis import elvis_plot  # pylint: disable=import-error

    # masses_star_wetzel2013 = [9.8, 10.0, 10.2, 10.4, 10.6, 10.8]  # , 11.0, 11.2]
    # qu_times_wetzel2013 = [
    #    [5.104542, 4.785763, 4.653718, 4.224645, 3.205032, 3.032932],
    #    [5.304676, 5.117624, 5.005337, 4.597469, 3.418093, 3.208626]
    # ]
    # masses_star_wheeler = [8.5, 9.5]
    # qu_times_wheeler = [9.5, 9.5]
    # masses_star_wheeler = 9.0
    # qu_times_wheeler = 9.5

    Say = ut.io.SayClass(plot_quench_time_v_mass)

    if np.isscalar(gal_host_names):
        gal_host_names = [gal_host_names]

    MassBin = ut.binning.BinClass(mass_limits, mass_width)
    QuFrac = ut.math.FractionClass([len(gal_host_names), MassBin.number], uncertainty_kind='beta')
    Statistic = ut.math.StatisticClass()

    # from stacking satellites in all ELVIS hosts
    qu_times = np.zeros([len(gal_host_names), MassBin.number])
    qu_times_err_gal = np.zeros([len(gal_host_names), MassBin.number, 2])

    # median across all ELVIS hosts
    qu_times_med = np.zeros([len(gal_host_names), MassBin.number])
    qu_times_med_err = np.zeros([len(gal_host_names), MassBin.number, 2])

    # uncertainty from limited satellite counts -> limited infall time sampling
    qu_times_res_err = np.zeros([len(gal_host_names), MassBin.number, 2])

    # satellites in primary host halo
    sis = elvis_plot.get_indices_of_subhalos(
        sub, mass_name, mass_limits, True, host_paired, simulation_names_exclude
    )

    # halo-to-halo scatter in ELVIS
    sub_host_names = list(np.unique(sub['host.name'][sis]))

    gis = ut.array.get_indices(gal[mass_name], mass_limits)
    gis = ut.array.get_indices(gal['host.distance.total'], [0, 300], gis)  # only within 300 kpc

    for gal_host_i, gal_host_name in enumerate(gal_host_names):
        if gal_host_name not in ['MW', 'M31']:
            gis_h = gis
        else:
            gis_h = ut.array.get_indices(gal['host.name'], gal_host_name, gis)

        for mi in range(MassBin.number):
            m_bin_limits = MassBin.get_bin_limits(mi)

            gis_m = ut.array.get_indices(gal[mass_name], m_bin_limits, gis_h)
            gis_m_qu = ut.array.get_indices(gal['gas.mass.ratio'], [0, gas_mass_ratio_qu], gis_m)
            qu_frac, qu_frac_err = QuFrac.get_fraction(gis_m_qu.size, gis_m.size)
            QuFrac.assign_to_dict([gal_host_i, mi], gis_m_qu.size, gis_m.size)

            sis_m = ut.array.get_indices(sub[mass_name], m_bin_limits, sis)

            Say.say('m = %s' % ut.array.get_limits(m_bin_limits, digit_number=2))

            # deal with incompleteness - issue at M_star < 1e6 M_sun
            Say.say(
                '  galaxy distance max {kpc}: for all = %.0f, for qu = %.0f'
                % (
                    gal['host.distance.total'][gis_m].max(),
                    gal['host.distance.total'][gis_m_qu].max(),
                )
            )

            # keep only satellites in simulation out to maximum distance observed in mass bin
            if use_distance_cut:
                distance_max = gal['host.distance.total'][gis_m].max() * 1.05
                if distance_max < 86:
                    distance_max = 86  # this gets to 10 SMC-mass satellites
                sis_m = ut.array.get_indices(sub['host.distance.total'], [0, distance_max], sis_m)

            Say.say('  galaxy number = %d' % gis_m.size)
            Say.say('  subhalo number = %d' % sis_m.size)

            Say.say(
                '  distance med: galaxy = %.2f, subhalo = %.2f'
                % (
                    np.median(gal['host.distance.total'][gis_m]),
                    np.median(sub['host.distance.total'][sis_m]),
                )
            )

            # sort from small to large time-since-infall
            inf_times = np.sort(sub[infall_property_name][sis_m])
            ac_percent = 100 * (1 - qu_frac)

            # quench time median - using interpolation for small number statistics
            qu_times[gal_host_i, mi] = np.percentile(inf_times, ac_percent)

            # uncertainty from finite width in infall time in subhalo sample
            qu_times_res_err[gal_host_i, mi] = [
                qu_times[gal_host_i, mi]
                - np.percentile(inf_times, ac_percent, interpolation='lower'),
                np.percentile(inf_times, ac_percent, interpolation='higher')
                - qu_times[gal_host_i, mi],
            ]
            # Say.say('  infall time resolution error = %s' % qu_times_res_err[gal_host_i, mi])

            # scatter via observed satellite counts
            ac_percent_hi = 100 * (1 - (qu_frac - qu_frac_err[0]))
            qu_times_err_gal[gal_host_i, mi, 1] = (
                np.percentile(inf_times, ac_percent_hi) - qu_times[gal_host_i, mi]
            )

            ac_percent_lo = 100 * (1 - (qu_frac + qu_frac_err[1]))
            qu_times_err_gal[gal_host_i, mi, 0] = qu_times[gal_host_i, mi] - np.percentile(
                inf_times, ac_percent_lo
            )

            # halo-to-halo scatter in ELVIS
            if gal_host_name not in ['MW', 'M31']:
                gal_number = gis_m.size
            else:
                gal_number = gis_m.size / 2

            counts = np.zeros(10000)
            qu_times_h = []
            for sub_host_name in sub_host_names:
                sis_h = ut.array.get_indices(sub['host.name'], sub_host_name, sis_m)
                counts[sis_h.size] += 1
                # ensure that given host has at least as many satellites as MW or M31
                if sis_h.size >= gal_number:
                    inf_times_h = np.sort(sub[infall_property_name][sis_h])
                    qu_times_h.append(np.percentile(inf_times_h, ac_percent))

            # for count_i, count in enumerate(counts):
            #    if count:
            #        Say.say('%d host has %d satellites' % (count, count_i))

            Statistic.stat = Statistic.get_statistic_dict(qu_times_h)
            qu_times_med[gal_host_i, mi] = Statistic.stat['median']
            qu_times_med_err[gal_host_i, mi] = Statistic.stat['median.difs.68']

    if np.max(mass_limits) == 10:
        MassBin.mids[-1] = 9.18  # LMC is only galaxy at this mass

    # plot ----------


def plot_proper_motion_uncertainty_v_distance(plot_file_name=None, plot_directory='.'):
    '''
    .
    '''
    _name = [
        'phx 1',
        'eri 2',
        'peg 3',
        'pis 2',
        'col 1',
        'hyd 2',
        'gru 1',
        'pic 1',
        'umaj 1',
        'ret 3',
        'vir 1',
        'eri 3',
        'phx 2',
        'hor 2',
    ]

    # heliocentric distance [kpc]
    distances = np.array([415, 366, 215, 187, 182, 134, 120, 114, 97.3, 92, 87, 87, 84, 78])

    # uncertainties from HST + JWST(2024) [km/s]
    jwst_uncs = np.array(
        [55.3, 43.7, 37.1, 25.8, 28.8, 17.4, 18.1, 16.2, 8.4, 17.8, 24.0, 14.0, 13.0, 10.5]
    )

    print(np.mean(jwst_uncs), np.median(jwst_uncs))

    # uncertainties from Gaia DR3 [km/s]
    gaia_dr3_uncs = np.array(
        [281, 630, 22.4, 461, 278, 283, 148, 153, 69.6, 185, 630, 108, 57.1, 237]
    )
    # expected uncertainties from Gaia DR4 [km/s]
    gaia_dr4_uncs = np.array(
        [98.5, 220, 7.8, 161, 97.2, 98.9, 51.8, 53.5, 24.4, 64.6, 630, 37.8, 20.0, 83.2]
    )

    print(np.mean(gaia_dr3_uncs / jwst_uncs), np.median(gaia_dr3_uncs / jwst_uncs))
    print(np.mean(gaia_dr4_uncs / jwst_uncs), np.median(gaia_dr4_uncs / jwst_uncs))

    gaia_star_numbers = np.array([0, 20, 4, 3, 6, 18, 9, 9, 44, 6, 0, 3, 10, 4])

    # plot ----------
    _fig, subplot = ut.plot.make_figure()

    ut.plot.set_axes_scaling_limits(
        subplot,
        True,
        [70, 460],
        None,
        True,
        [6.5, 740],
    )

    # subplot.xaxis.set_minor_locator(ticker.AutoMinorLocator(4))
    subplot.xaxis.set_ticks([80, 100, 130, 200, 300, 400])
    subplot.xaxis.set_major_formatter(ticker.FormatStrFormatter('%.0f'))

    subplot.yaxis.set_ticks([7, 10, 20, 30, 50, 100, 200, 300, 500])
    subplot.yaxis.set_major_formatter(ticker.FormatStrFormatter('%.0f'))

    subplot.set_xlabel('distance [kpc]')
    subplot.set_ylabel('PM uncertainty [km/s]')

    for di, d in enumerate(distances):
        subplot.plot(
            [d, d, d], [jwst_uncs[di], gaia_dr4_uncs[di], gaia_dr3_uncs[di]], c='black', alpha=0.3
        )

    subplot.plot(
        distances,
        jwst_uncs,
        color='black',
        alpha=0.9,
        marker='s',
        markersize=8,
        linestyle='',
        label='HST+JWST',
    )

    masks = gaia_star_numbers > 9

    subplot.plot(
        distances[masks],
        gaia_dr3_uncs[masks],
        color='orange',
        alpha=0.8,
        marker='o',
        markersize=8,
        linestyle='',
        label='Gaia DR3',
    )

    subplot.plot(
        distances[masks],
        gaia_dr4_uncs[masks],
        color='orange',
        alpha=0.3,
        marker='o',
        markersize=7,
        markerfacecolor='none',
        linestyle='',
        label='Gaia DR4(est.)',
    )

    masks = gaia_star_numbers <= 9

    subplot.plot(
        distances[masks],
        gaia_dr3_uncs[masks],
        color='red',
        alpha=0.8,
        marker='d',
        markersize=8,
        linestyle='',
        label='Gaia $N_{{star}}<10$',
    )

    subplot.plot(
        distances[masks],
        gaia_dr4_uncs[masks],
        color='red',
        alpha=0.3,
        marker='d',
        markersize=7,
        markerfacecolor='none',
        linestyle='',
    )

    ut.plot.make_legends(subplot, bbox_to_anchor=[0.58, 0.345])

    ut.plot.parse_output(plot_file_name, plot_directory)
