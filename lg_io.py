'''
Read data for galaxies in the Local Group.

@author: Andrew Wetzel <arwetzel@gmail.com>

----------
Units

Unless otherwise noted, this package stores all quantities in (combinations of) these base units
    mass [M_sun]
    position [kpc comoving]
    distance, radius [kpc physical]
    time [Gyr]
    temperature [K]
    magnetic field [Gauss]
    elemental abundance [linear mass fraction]

These are the common exceptions to those standards
    velocity [km/s]
    acceleration [km/s / Gyr]
    gravitational potential [km^2 / s^2]
    rates (star formation, cooling, accretion) [M_sun / yr]
    metallicity (if converted from stored massfraction)
        [log10(mass_fraction / mass_fraction_solar)], using Asplund et al 2009 for Solar
'''

import os
import numpy as np

import utilities as ut


# --------------------------------------------------------------------------------------------------
# galaxies in the Local Group
# --------------------------------------------------------------------------------------------------
class GalaxyDictionaryClass(dict):
    '''
    Dictionary class to store galaxy properties.
    Allows greater flexibility for producing derived quantities.
    '''

    def __init__(self):
        self.info = {}

    def prop(self, property_name, indices=None):
        '''
        Get property, either from self dictionary or derive.
        If several properties, need to provide mathematical relationship.

        Parameters
        ----------
        property_name : str
            name of property
        indices : array
            indices to select on (of arbitrary dimensions)
        '''
        # parsing general to all catalogs ##
        property_name = property_name.strip()  # strip white space

        # if input is in self dictionary, return as is
        if property_name in self:
            if indices is not None:
                return self[property_name][indices]
            else:
                return self[property_name]

        # math relation, combining more than one property
        if (
            '/' in property_name
            or '*' in property_name
            or '+' in property_name
            or '-' in property_name
        ):
            prop_names = property_name

            for delimiter in ['/', '*', '+', '-']:
                if delimiter in property_name:
                    prop_names = prop_names.split(delimiter)
                    break

            if len(prop_names) == 1:
                raise KeyError(f'property = {property_name} is not valid input to {self.__class__}')

            prop_values = self.prop(prop_names[0], indices)
            if not np.isscalar(indices):
                # make copy so not change values in input catalog
                prop_values = np.array(prop_values)

            for prop_name in prop_names[1:]:
                if '/' in prop_name:
                    if np.isscalar(prop_values):
                        if self.prop(prop_name, indices) == 0:
                            prop_values = np.nan
                        else:
                            prop_values = prop_values / self.prop(prop_name, indices)
                    else:
                        masks = self.prop(prop_name, indices) != 0
                        prop_values[masks] /= self.prop(prop_name, indices)[masks]
                        masks = self.prop(prop_name, indices) == 0
                        prop_values[masks] = np.nan
                if '*' in prop_name:
                    prop_values *= self.prop(prop_name, indices)
                if '+' in prop_name:
                    prop_values += self.prop(prop_name, indices)
                if '-' in prop_name:
                    prop_values -= self.prop(prop_name, indices)

            # if prop_values.size == 1:
            #    prop_values = np.float64(prop_values)

            return prop_values

        # math transformation of single property
        if property_name[:3] == 'log':
            return ut.math.get_log(self.prop(property_name.replace('log', ''), indices))

        if property_name[:3] == 'abs':
            return np.abs(self.prop(property_name.replace('abs', ''), indices))

        # average stellar density
        if 'star.density.' in property_name:
            if property_name == 'star.density':
                property_name += '.50'  # use R_50 as default radius to measure stellar density

            radius_percent = float(property_name.split('.')[-1])
            radius_name = 'star.radius.' + property_name.split('.')[-1]

            return (
                radius_percent
                / 100
                * self['star.mass'][indices]
                / (4 / 3 * np.pi * self[radius_name][indices] ** 3)
            )

        if 'gas.mass.ratio' in property_name:
            # treat upper limits as 0 gas mass
            values = np.array(self['star.mass']) * 0
            masks = self['gas.mass.limit'] == 0
            values[masks] = self['gas.mass'][masks] / self['star.mass'][masks]
            return values

        # if get this far without return, problem
        raise KeyError(f'property = {property_name} is not valid input to {self.__class__}')


class LocalGroupClass(ut.io.SayClass):
    '''
    Read catalog of observed properties of dwarf galaxies in the Local Group.
    '''

    def read(self, file_path='./', file_name='localgroup_galaxies.txt', remove_names=None):
        '''
        Returns
        -------
        gal : dict
            catalog of galaxies in the Local Group
        '''
        if file_path is None:
            file_name = os.path.expanduser('~') + '/work/research/observation/local_group/'
            file_name += 'localgroup_galaxies.txt'
        else:
            file_name = file_path + file_name

        gal_read = np.loadtxt(
            file_name,
            encoding='utf-8',
            comments='#',
            delimiter=',',
            dtype=[
                ('name', '|S40'),
                ('host.name', '|S10'),
                ('host.distance.total', np.float32),
                ('host.distance.total.err', np.float32),
                ('sun.distance.total', np.float32),
                ('sun.distance.total.err', np.float32),
                ('star.mass', np.float32),
                ('star.radius.50', np.float32),  # observed 2-D half-light radius
                ('star.radius.50.err', np.float32),
                ('star.vel.std.1d', np.float32),  # observed 1-D velocity dispersion
                ('star.vel.std.1d.err', np.float32),
                ('total.mass.50', np.float32),
                ('total.mass.50.err.hi', np.float32),
                ('total.mass.50.err.lo', np.float32),
                ('gas.mass', np.float32),
                ('gas.mass.limit', np.int32),
                ('star.metallicity.iron', np.float32),
                ('star.metallicity.iron.std', np.float32),
            ],
        )

        gal = GalaxyDictionaryClass()

        for k in gal_read.dtype.names:
            gal[k] = gal_read[k]

            if 'radius' in k:
                gal[k] /= 1000  # convert to [kpc]

        for remove_name in remove_names:
            masks = gal['name'] != remove_name.encode()
            for k in gal:
                gal[k] = gal[k][masks]

        # assign V_circ from dynamical mass
        for value_kind in ['', '.err.hi', '.err.lo']:
            gal['vel.circ.50' + value_kind] = np.array(gal['total.mass.50'])
        masks = np.isfinite(gal['total.mass.50'])
        gal['vel.circ.50'][masks] = np.sqrt(
            gal['total.mass.50'][masks] / gal['star.radius.50'][masks]
        )
        gal['vel.circ.50.err.hi'][masks] = np.sqrt(
            (gal['total.mass.50'][masks] + gal['total.mass.50.err.hi'][masks])
            / gal['star.radius.50'][masks]
        ) - np.sqrt(gal['total.mass.50'][masks] / gal['star.radius.50'][masks])
        gal['vel.circ.50.err.lo'][masks] = np.sqrt(
            gal['total.mass.50'][masks] / gal['star.radius.50'][masks]
        ) - np.sqrt(
            (gal['total.mass.50'][masks] - gal['total.mass.50.err.lo'][masks])
            / gal['star.radius.50'][masks]
        )
        for value_kind in ['', '.err.hi', '.err.lo']:
            gal['vel.circ.50' + value_kind] *= (
                np.sqrt(ut.constant.grav_kpc_msun_yr)
                * ut.constant.km_per_kpc
                * ut.constant.yr_per_sec
            )

        # compile pointer from name to index
        gal['name.to.index'] = {}
        for name_i, name in enumerate(gal['name']):
            gal['name.to.index'][name.decode().lower()] = name_i

        # compute 3-D dispersion
        gal['star.vel.std'] = np.sqrt(3) * gal['star.vel.std.1d']
        gal['star.vel.std.err'] = np.sqrt(3) * gal['star.vel.std.1d.err']

        # pointers for comparing to simulated galaxies
        gal['star.vel.std.50.1d'] = gal['star.vel.std.1d']
        gal['star.vel.std.50'] = gal['star.vel.std']

        gal.info = {'catalog.kind': 'galaxy', 'catalog.source': 'local group'}

        return gal


LocalGroup = LocalGroupClass()


class LocalGroupMatchClass(ut.io.SayClass):
    '''
    Read catalog of dwarf galaxies in the Local Group, for matching subhalos in ELVIS.
    '''

    def read(self):
        '''
        Returns
        -------
        gal : dict
            catalog of galaxies in local group
        '''
        file_name = os.path.expanduser('~') + '/work/research/project/lefse/satellite_orbit/'
        file_name += 'galaxies_selection.txt'

        gal_read = np.loadtxt(
            file_name,
            encoding='utf-8',
            delimiter=',',
            comments='#',
            dtype=[
                ('name', 'S20'),
                ('snapshot.min', np.int32),
                ('snapshot.max', np.int32),
                ('log star.mass', np.float32),
                ('log star.mass.err', np.float32),
                ('host.distance.total', np.float32),
                ('host.distance.total.err', np.float32),
                ('host.velocity.rad', np.float32),
                ('host.velocity.rad.err', np.float32),
                ('host.velocity.tan', np.float32),
                ('host.velocity.tan.err', np.float32),
            ],
        )

        # transfer to dictionary
        gal = GalaxyDictionaryClass()

        for name_i, name in enumerate(gal_read['name']):
            key_name = name.decode()
            key_name = key_name.lower()
            gal[key_name] = {}
            for prop_name in gal_read.dtype.names:
                gal[key_name][prop_name] = gal_read[prop_name][name_i]
                if isinstance(gal[key_name][prop_name], bytes):
                    gal[key_name][prop_name] = gal[key_name][prop_name].decode()
                elif np.isnan(gal[key_name][prop_name]):
                    gal[key_name][prop_name] = None

                # if 'log ' in prop and '.err' not in prop:
                #    gal[key_name][prop.replace('log ', '')] = 10 ** gal[key_name][prop_name]

        # convert snapshot limits to range
        for name in gal:
            gal[name]['snapshots'] = list(
                range(gal[name]['snapshot.min'], gal[name]['snapshot.max'] + 1)
            )
            del (gal[name]['snapshot.min'], gal[name]['snapshot.max'])

        return gal


class SFHClass(ut.io.SayClass):
    '''
    Read star-formation historyes of dwarf galaxies in the Local Group.
    '''

    def read(self):
        '''
        Returns
        -------
        gals : dict
            catalog of galaxies in the Local Group
        '''
        time_max = 13.75

        directory = (
            os.path.expanduser('~')
            + '/work/research/observation/local_group/star_formation_histories/'
        )

        file_name_base = directory + '*.txt'

        file_names = ut.io.get_file_names(file_name_base)

        gals = GalaxyDictionaryClass()

        for file_name in file_names:
            gal_read = np.loadtxt(
                file_name,
                encoding='utf-8',
                comments='#',
                usecols=[0, 1, 12],
                dtype=[
                    ('time.lookback.min', np.float32),
                    ('time.lookback.max', np.float32),
                    ('mass.normalized.bin', np.float32),
                ],
            )

            gal = {}
            for prop_name in gal_read.dtype.names:
                gal[prop_name] = gal_read[prop_name]

            gal['time.lookback.mid'] = 0.5 * (gal['time.lookback.max'] + gal['time.lookback.min'])

            for prop_name in list(gal):
                if 'time' in prop_name:
                    gal[prop_name] = 10 ** gal[prop_name] / ut.constant.giga  # [Gyr]

                gal['time.lookback'] = []
                gal['mass.normalized'] = []
                for ti in range(gal['time.lookback.min'].size):
                    # consistent with Dan's figures
                    gal['time.lookback'].append(gal['time.lookback.min'][ti])
                    gal['mass.normalized'].append(gal['mass.normalized.bin'][ti])

                gal['time.lookback'].append(time_max)
                gal['mass.normalized'].append(0)

                gal['time.lookback'] = np.array(gal['time.lookback'])
                gal['mass.normalized'] = np.array(gal['mass.normalized'])

            gal_name = file_name.split('/')[-1].replace('_', ' ').replace('.txt', '')
            gals[gal_name] = gal

        gals.info = {'catalog.kind': 'sfh', 'catalog.source': 'local group'}

        return gals
